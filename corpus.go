package corpus

import (
	"regexp"
	"runtime"
	"strings"
	"time"
	"unicode/utf8"
)

func MakeRune(data string) []string {
	out := make([]string, 0)
	for _, txt := range data {
		buf := make([]byte, 10)
		n := utf8.EncodeRune(buf, txt)
		out = append(out, string(buf[:n]))
	}
	return out
}

func MakeString(data []string) string {
	out := ""
	for _, txt := range data {
		out = out + txt
	}
	return out
}

func changeTOC(data string) string {
	r := regexp.MustCompile(`<div id="toc" class="toc">`)
	return r.ReplaceAllString(data, "<toc>")
}

func TagFindFunc(data string, datarune []string) (string, string, string, []string) {
	tagName := ""
	tagType := "none"
	fdata := data

	//	ldata := ""
	ldata := []string{}

	//	wdata := MakeRune(data)
	wdata := datarune

	wsize := len(wdata)
	pointer := 0

	tagFind := 0

LoopFindTag:
	for {
		if pointer >= wsize {
			break
		}

		c := strings.ToLower(wdata[pointer])
		pointer++

		switch tagFind {
		case 0:

			if c == "<" {
				tagFind = 1
				fdata = MakeString(wdata[:pointer-1])
			}

		case 1:

			if c == "/" {
				tagType = "close"
				tagFind = 2
			} else if c >= "a" && c <= "z" {
				tagType = "open"
				tagName = tagName + c
				tagFind = 2
			} else if c == ">" {
				pointer--
				tagFind = 3
			}

		case 2:

			if c >= "a" && c <= "z" {
				tagName = tagName + c
			} else if c == ">" {
				pointer--
				tagFind = 3
			} else {
				tagFind = 3
			}

		case 3:

			if c == ">" {
				// ldata = MakeString(wdata[pointer:])
				ldata = wdata[pointer:]
				break LoopFindTag
			}

		}

	}

	return tagName, tagType, fdata, ldata
}

func CleanCorpus(data string) string {
	cleandata := RemoveHTML(data)
	cleandata = RemoveDoubleWhiteSpace(cleandata)
	cleandata = RemoveUnusedPattern(cleandata)
	cleandata = RemoveDoubleWhiteSpace(cleandata)
	cleandata = CovertSpecialCharactor(cleandata)
	return cleandata
}

func RemoveHTML(datax string) string {
	data := changeTOC(datax)
	timerX := time.NewTicker(time.Second)
	defer timerX.Stop()

	dout := ""
	text := MakeRune(data)
	blockTag := ""
	blockTagN := 0

LoopRemoveHTML:
	for {
		select {
		case <-timerX.C:
			runtime.Gosched()
		default:
		}
		tagName, tagType, fdata, ldata := TagFindFunc(data, text)
		switch blockTag {
		case "":
			switch tagType {
			case "none":
				//dout = dout + fdata
				break LoopRemoveHTML
			case "open":
				switch tagName {
				case "br", "li", "ol", "ul":
					dout = dout + fdata + "\n"
					text = ldata
				case "head", "title", "style", "script", "table", "toc":
					blockTag = tagName
					if tagName == "toc" {
						blockTag = "div"
					}
					blockTagN = 1
					dout = dout + fdata + "\n"
					text = ldata
				default:
					dout = dout + fdata
					text = ldata
				}
			case "close":
				switch tagName {
				case "li", "ol", "ul":
					dout = dout + fdata + "\n"
					text = ldata
				default:
					dout = dout + fdata
					text = ldata
				}
			}
		default:
			switch tagType {
			case "open":
				switch tagName {
				case "head", "title", "style", "script", "table", "div":
					if blockTag == tagName {
						blockTagN++
					}
					text = ldata
				default:
					text = ldata
				}
			case "close":
				switch tagName {
				case "head", "title", "style", "script", "table", "div":
					if blockTag == tagName {
						blockTagN--
						if blockTagN == 0 {
							text = ldata
							blockTag = ""
						} else {
							text = ldata
						}
					} else {
						text = ldata
					}
				default:
					text = ldata
				}

			}
		}
	}

	return dout
}

func RemoveDoubleWhiteSpace(data string) string {
	r := regexp.MustCompile(`[ \t]+`)
	return r.ReplaceAllString(data, " ")
}

func RemoveUnusedPattern(data string) string {
	r := regexp.MustCompile(`\[แก้\]`)
	datax := r.ReplaceAllString(data, "")

	r = regexp.MustCompile(`\[\d+\]:[ุ\divx–-]+`)
	datax = r.ReplaceAllString(datax, "")

	r = regexp.MustCompile(`&#\d+;\d+&#\d+;:[ุ\divx–-]+`)
	datax = r.ReplaceAllString(datax, "")

	r = regexp.MustCompile(`\[\d+\]`)
	datax = r.ReplaceAllString(datax, "")

	r = regexp.MustCompile(`&#\d+;\d+&#\d+;`)
	datax = r.ReplaceAllString(datax, "")

	r = regexp.MustCompile(`&#\d+;ต้องการอ้างอิง&#\d+;`)
	datax = r.ReplaceAllString(datax, "")

	r = regexp.MustCompile(`\[ต้องการอ้างอิง\]`)
	datax = r.ReplaceAllString(datax, "")

	r = regexp.MustCompile(`\[เชิงอรรถ \d+\]`)
	datax = r.ReplaceAllString(datax, "")

	r = regexp.MustCompile(`&#\d+;เชิงอรรถ \d+&#\d+;`)
	datax = r.ReplaceAllString(datax, "")

	r = regexp.MustCompile("\nดูบทความหลักที่:.+\n")
	datax = r.ReplaceAllString(datax, "\n")

	r = regexp.MustCompile("\nดูเพิ่มเติมที่:.+\n")
	datax = r.ReplaceAllString(datax, "\n")

	r = regexp.MustCompile("\nเชิงอรรถ\n")
	loc := r.FindStringIndex(datax)
	data11 := ""
	if loc == nil {
		data11 = datax
	} else {
		data11 = datax[:loc[0]]
	}

	r = regexp.MustCompile("\nอ้างอิง\n")
	loc2 := r.FindStringIndex(data11)
	if loc2 == nil {
		return data11
	}
	return data11[:loc2[0]]
}

func CovertSpecialCharactor(data string) string {
	lines := strings.Split(data, "\n")
	returnText := ""
	for _, v := range lines {
		text := strings.TrimSpace(v)
		if text == "" {
			continue
		}
		newtext := ""
		for _, vv := range text {
			vvx := string(vv)
			switch vvx {
			case "<":
				newtext += "<less_than>"
			case ">":
				newtext += "<greater_than>"
			case " ":
				newtext += "<space>"
			case "[":
				newtext += "<left_square_bracket>"
			case "]":
				newtext += "<right_square_bracket>"
			case "(":
				newtext += "<left_parenthesis>"
			case ")":
				newtext += "<right_parenthesis>"
			case "{":
				newtext += "<left_curly_bracket>"
			case "}":
				newtext += "<right_curly_bracket>"
			case "!":
				newtext += "<exclamation>"
			case "′":
				newtext += "<prime>"
			case `"`:
				newtext += "<quotation>"
			case "`":
				newtext += "<grave_accent>"
			case "#":
				newtext += "<number>"
			case "/":
				newtext += "<slash>"
			case `\`:
				newtext += "<back_slash>"
			case ":":
				newtext += "<colon>"
			case ";":
				newtext += "<semi_colon>"
			case "$":
				newtext += "<dollar>"
			case "%":
				newtext += "<percent>"
			case "&":
				newtext += "<ampersand>"
			case "'":
				newtext += "<apostrophe>"
			case "*":
				newtext += "<asterisk>"
			case "+":
				newtext += "<plus>"
			case ",":
				newtext += "<comma>"
			case "-":
				newtext += "<minus>"
			case ".":
				newtext += "<full_stop>"
			case "=":
				newtext += "<equal>"
			case "?":
				newtext += "<question_mark>"
			case "@":
				newtext += "<at_mark>"
			case "^":
				newtext += "<circumflex_accent>"
			case "_":
				newtext += "<low_line>"
			case "~":
				newtext += "<tilde>"
			case "–":
				newtext += "<en_dash>"
			case "°":
				newtext += "<degree>"
			default:
				newtext += vvx
			}
		}
		returnText += newtext + "<end>\n"
	}
	return returnText
}

func CleanSentence(data string) string {
	datax := strings.Split(data, "\n")
	datanew := make([]string, 0)
	for i := range datax {
		if len(datax[i]) == 0 {
			continue
		}
		datanew = append(datanew, datax[i])
	}

	return strings.Join(datanew, "\n")
}
